from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_get_articles():
  

    response = client.get("/fetch")
    assert response.status_code == 200
    assert response.json() == {
         "data": [
            {
            "Title": "title1",
            "desc": ""
            },
            {
            "Tite": "title2",
            "desc": ""
            }
        ]

}
